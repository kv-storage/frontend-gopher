package main

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"

	"nvidia.com/kv-frontend/model"
	"nvidia.com/kv-frontend/state"

	"honnef.co/go/js/dom"
	"honnef.co/go/js/xhr"
)

func getBackendURL() string {
	var document = dom.GetWindow().Document().(dom.HTMLDocument)
	u, _ := url.Parse(document.URL())
	if u.Hostname() == "localhost" {
		return "http://localhost:5000/"
	}
	return "https://kv-backend.zhk.host/"

}

// Add to the Error State a new position with
// information about current error
func displayError(text string) {
	currErrors := state.State.Root().Errors().Get().Range()
	newErrors := append(currErrors, model.NewError(text))
	fmt.Println(newErrors)
	state.State.Root().Errors().Set(
		model.NewErrors(newErrors...))
}

// Add key to the database, if succes also push it to
// the global Entry state, else display error
func createKey(jsonData []byte, st CreateModalState) {
	req := xhr.NewRequest("POST", getBackendURL()+"createKey")
	req.ResponseType = xhr.JSON
	req.SetRequestHeader("Content-Type", "application/json")
	req.Send(jsonData)
	if req.Status != 200 {
		displayError(strconv.Itoa(req.Status) + ": " + req.StatusText)
	} else {
		currEntrs := state.State.Root().Entries().Get().Range()
		newEntrs := append(currEntrs, model.NewEntry(st.keyName, st.keyValue))
		state.State.Root().Entries().Set(
			model.NewEntries(newEntrs...))
	}

}

// Delete key to the database, if succes also delete it from
// the global Entry state, else display error
func deleteKey(keyName string) {
	req := xhr.NewRequest("DELETE", getBackendURL()+"deleteKey/"+keyName)
	req.ResponseType = xhr.JSON
	req.SetRequestHeader("Content-Type", "application/json")
	req.Send(nil)
	if req.Status != 200 {
		displayError(strconv.Itoa(req.Status) + ": " + req.StatusText)
	} else {
		currEntrs := state.State.Root().Entries().Get().Range()
		var newEntrs []*model.Entry
		for _, j := range currEntrs {
			if j.KeyName() != keyName {
				newEntrs = append(newEntrs, j)
			}
		}
		state.State.Root().Entries().Set(
			model.NewEntries(newEntrs...))
	}
}

// Get all the keys from the database, add them to
// the global Entry state
func listStorage() {
	req := xhr.NewRequest("GET", getBackendURL()+"listStorage/")
	// Even though we are requsting a JSON, we need to get it as plain text
	// in order to be able to convert in to `map[string]string`
	// with `json.Unmarshal()`
	req.ResponseType = xhr.Text
	req.SetRequestHeader("Content-Type", "content type text/plain")
	err := req.Send(nil)
	if err != nil {
		fmt.Println("Error ", err)
	}
	var mapRes map[string]interface{}
	json.Unmarshal([]byte(req.Response.String()), &mapRes)

	form := make(map[string]string)
	for k, v := range mapRes {
		// We need to convert all floats to strings in order to be able to
		// display render them in React component
		switch v.(type) {
		case string:
			form[k] = v.(string)
		case float64:
			form[k] = strconv.FormatFloat(v.(float64), 'f', 6, 64)
		}
	}
	var formatedEntries []*model.Entry
	for i, j := range form {
		formatedEntries = append(formatedEntries, model.NewEntry(i, j))
	}

	newEntr := model.NewEntries(formatedEntries...)
	state.State.Root().Entries().Set(newEntr)

}
