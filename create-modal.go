package main

import (
	"encoding/json"
	"strconv"

	"honnef.co/go/js/dom"
	"myitcv.io/react"
)

type CreateModalDef struct {
	react.ComponentDef
}
type CreateModalState struct {
	keyName      string
	selectedType string
	keyValue     string
}
type CreateModalProps struct {
	CreateModalState
}

func CreateModal() *CreateModalElem {
	return buildCreateModalElem(CreateModalProps{})
}

// On component initiation, set default type as `string`
func (c CreateModalDef) ComponentWillMount() {

	c.SetState(CreateModalState{selectedType: "String"})
}

// A bunch of React elements
func (c CreateModalDef) renderCard(e *react.InputElem) react.Element {
	return react.Div(&react.DivProps{ClassName: "card create-entry"},
		react.Div(&react.DivProps{ClassName: "card-body"},
			c.renderCardBody(e),
		),
	)
}

func (c CreateModalDef) renderCardBody(e *react.InputElem) react.Element {
	keyName := c.State().keyName
	selectValue := c.State().selectedType
	return react.Div(&react.DivProps{ClassName: "create-entry"},
		react.H3(nil, react.S("Add Position")),
		react.Div(&react.DivProps{ClassName: "input-group"},

			react.Input(&react.InputProps{Type: "text", Placeholder: "Key Name", Value: keyName, OnChange: changeKey{c}}),
			react.Select(&react.SelectProps{Value: selectValue, OnChange: changeType{c}},
				react.Option(nil, react.S("")),
				react.Option(nil, react.S("String")),
				react.Option(nil, react.S("Number")),
			),
			e,
		),
		react.Div(nil,
			react.Button(&react.ButtonProps{ClassName: "btn btn-primary add-entry", OnClick: addEntry{c}}, react.S("Add Entry")),
		))
}

// Change current entry type on change in  changeType field
type changeType struct{ c CreateModalDef }

func (ct changeType) OnChange(se *react.SyntheticEvent) {
	st := ct.c.State()

	target := se.Target().(*dom.HTMLSelectElement)
	st.selectedType = target.Value

	ct.c.SetState(st)
}

// Change current entry type on change in changeKey field
type changeKey struct{ c CreateModalDef }

func (ck changeKey) OnChange(se *react.SyntheticEvent) {
	st := ck.c.State()

	target := se.Target().(*dom.HTMLInputElement)
	st.keyName = target.Value

	ck.c.SetState(st)
}

// Change current entry value on change in changeValue field
type changeValue struct{ c CreateModalDef }

func (cv changeValue) OnChange(se *react.SyntheticEvent) {
	st := cv.c.State()

	target := se.Target().(*dom.HTMLInputElement)
	st.keyValue = target.Value

	cv.c.SetState(st)

}

// Add entry on click on `addEntry` button
type addEntry struct{ c CreateModalDef }

func (ae addEntry) OnClick(se *react.SyntheticMouseEvent) {
	st := ae.c.State()
	// Field value is always `string`, so we need
	// to convert it to required type manually
	if ae.c.State().selectedType == "Number" {
		m := make(map[string]float64)
		numberedValue, _ := strconv.ParseFloat(st.keyValue, 64)
		m[st.keyName] = numberedValue
		jsonData, error := json.Marshal(m)
		if error != nil {
			panic(error)
		}
		go createKey(jsonData, st)
	} else {
		m := make(map[string]string)
		m[st.keyName] = st.keyValue
		jsonData, error := json.Marshal(m)
		if error != nil {
			panic(error)
		}

		go createKey(jsonData, st)
	}
	// Reset form fields
	ae.c.SetState(CreateModalState{"", "String", ""})

}

func (c CreateModalDef) Render() react.Element {
	keyValue := c.State().keyValue
	textDomEl := react.Input(&react.InputProps{Type: "text", ClassName: "form-control", Value: keyValue, OnChange: changeValue{c}})
	numberDomEl := react.Input(&react.InputProps{Type: "number", ClassName: "form-control", Value: keyValue, OnChange: changeValue{c}})

	var currentEl *react.InputElem
	var elems []react.Element

	switch c.State().selectedType {
	case "String":
		currentEl = textDomEl
	case "Number":
		currentEl = numberDomEl

	}
	elems = append(elems, c.renderCard(currentEl))

	return react.Div(nil,
		elems...,
	)
}
