package main

import (
	"myitcv.io/react"
	"nvidia.com/kv-frontend/model"
	"nvidia.com/kv-frontend/state"
)

type AppDef struct {
	react.ComponentDef
}

type EntryState interface {
	Get() *model.Entries
	Set(p *model.Entries)
	Subscribe(cb func()) *state.Sub
}

type EntriesState struct {
	Entries    *model.Entries
	EntriesSub *state.Sub
}

type ErrorState interface {
	Get() *model.Errors
	Set(p *model.Errors)
	Subscribe(cb func()) *state.Sub
}
type ErrorsState struct {
	Errors    *model.Errors
	ErrorsSub *state.Sub
}
type AppProps struct {
	EntryState
	ErrorState
}
type AppState struct {
	currEntries    *model.Entries
	currEntriesSub *state.Sub

	currErrors    *model.Errors
	currErrorsSub *state.Sub
}

func (a AppDef) ComponentWillMount() {
	// Get list of entries from the server
	go listStorage()
	// Mark `EntriesStateChanged` and `ErrorsStateChanged` for
	// execution on change in global state
	entSub := a.Props().EntryState.Subscribe(a.EntriesStateChanged)
	errSub := a.Props().ErrorState.Subscribe(a.ErrorsStateChanged)
	// Bind subscribtions to the current component scope
	st := a.State()
	st.currEntriesSub = entSub
	st.currErrorsSub = errSub
	st.currEntries = a.Props().EntryState.Get()
	st.currErrors = a.Props().ErrorState.Get()

	a.SetState(st)
}
func (a AppDef) EntriesStateChanged() {
	// Update local state on changes in global
	s := a.State()
	s.currEntries = a.Props().EntryState.Get()
	a.SetState(s)
}
func (a AppDef) ErrorsStateChanged() {
	// Update local state on changes in global
	s := a.State()
	s.currErrors = a.Props().ErrorState.Get()
	a.SetState(s)
}
func App(props AppProps) *AppElem {
	return buildAppElem(props)
}

// A bunch of React elements
func (a AppDef) renderCard(cardContent react.Element) react.Element {
	return react.Div(&react.DivProps{ClassName: "card"},
		react.Div(&react.DivProps{ClassName: "card-body"},
			cardContent,
		),
	)
}
func (a AppDef) renderErrorCardEmpty(errorData *model.Error) react.Element {
	return react.Div(&react.DivProps{ClassName: "card text-white bg-danger mb-3"},
		react.Div(&react.DivProps{ClassName: "card-header"}, react.S("Error"),

			react.Div(&react.DivProps{ClassName: "card-body"},
				react.P(&react.PProps{ClassName: "card-text"}, react.S("Server responded with code "+errorData.ErrorText())),
			),
			react.Div(&react.DivProps{ClassName: "card-footer"},
				react.Button(&react.ButtonProps{ClassName: "btn btn-danger", OnClick: deleteError{errorData}},
					react.S("Close Error Message"),
				)),
		),
	)

}

func (a AppDef) renderCardEmpty() react.Element {
	return react.Div(&react.DivProps{ClassName: "empty-storage"},
		react.I(&react.IProps{ClassName: "material-icons"}, react.S("code")),
		react.H1(nil, react.S("There are no entries in the KV Storage")),
		react.P(nil, react.S("Add one to view the list")),
	)
}

func (a AppDef) renderHeader() react.Element {
	return react.Nav(&react.NavProps{ClassName: "navbar navbar-expand-lg navbar-dark bg-primary"},
		react.A(&react.AProps{ClassName: "navbar-brand"}, react.S("Simple KV Storage Web UI")),
	)
}

func (a AppDef) renderEntriesTable(entries []react.Element) react.Element {
	return react.Div(&react.DivProps{ClassName: "table"},
		react.Div(&react.DivProps{ClassName: "entry-headers"},
			react.Div(&react.DivProps{ClassName: "entry-key"}, react.S("Key")),
			react.Div(&react.DivProps{ClassName: "entry-value"}, react.S("Value")),
		),
		react.Div(nil, entries...),
	)
}
func (a AppDef) renderEntryPosition(entryData *model.Entry) react.Element {
	return react.Div(&react.DivProps{ClassName: "entry-position"},
		react.Div(&react.DivProps{ClassName: "entry-key"}, react.S(entryData.KeyName())),
		react.Div(&react.DivProps{ClassName: "entry-value"}, react.S(entryData.KeyValue())),
		react.Div(&react.DivProps{ClassName: "delete-entry", OnClick: deleteEntry{entryData}},
			react.I(&react.IProps{ClassName: "material-icons"}, react.S("close")),
		))

}
func (a AppDef) CreateModal() react.Element {
	return react.Div(nil, CreateModal())
}

// Delete entry on click on corresponding `delete` button
type deleteEntry struct{ entryData *model.Entry }

func (de deleteEntry) OnClick(se *react.SyntheticMouseEvent) {
	go deleteKey(de.entryData.KeyName())
}

// Delete error on click on corresponding `delete` button
type deleteError struct{ errorData *model.Error }

func (de deleteError) OnClick(se *react.SyntheticMouseEvent) {
	// Get current state, exclude selected error from it
	// and save it as a new one
	errState := state.State.Root().Errors().Get().Range()
	var newState []*model.Error
	for _, err := range errState {
		if de.errorData.ErrorText() != err.ErrorText() {
			newState = append(newState, err)
		}
	}
	state.State.Root().Errors().Set(model.NewErrors(newState...))

}

func (a AppDef) Render() react.Element {

	var elems []react.Element
	// Add Header to the Template
	elems = append(elems, a.renderHeader())
	// Check the State for current entries, display content accordingly
	if a.State().currEntries.Len() != 0 {
		// For each entry in the state create a DOM element
		var entriesEls []react.Element
		for _, entryData := range a.State().currEntries.Range() {
			entriesEls = append(entriesEls, a.renderEntryPosition(entryData))
		}
		elems = append(elems, a.renderCard(a.renderEntriesTable(entriesEls)))
	} else {
		// Display `No entries, add one` message
		elems = append(elems, a.renderCard(a.renderCardEmpty()))

	}
	// If we have any errors in the state, create error message
	// for every single one of them
	if a.State().currErrors.Len() != 0 {
		for _, err := range a.State().currErrors.Range() {
			elems = append(elems, a.renderErrorCardEmpty(err))

		}
	}
	// Create modal for new entries creation
	elems = append(elems, CreateModal())

	return react.Div(nil, elems...)
}
