package model

//go:generate immutableGen

type _Imm_Entry struct {
	KeyName  string
	KeyValue string
}
type _Imm_Entries []*Entry

func NewEntry(keyName, keyValue string) *Entry {
	return new(Entry).WithMutable(func(e *Entry) {
		e.SetKeyName(keyName)
		e.SetKeyValue(keyValue)
	})
}
