package model

//go:generate immutableGen

type _Imm_Error struct {
	ErrorText string
}
type _Imm_Errors []*Error

func NewError(errorText string) *Error {
	return new(Error).WithMutable(func(e *Error) {
		e.SetErrorText(errorText)
	})
}
