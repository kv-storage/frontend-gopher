package main

import (
	"honnef.co/go/js/dom"
	"myitcv.io/react"
	"nvidia.com/kv-frontend/state"
)

//go:generate reactGen

var document = dom.GetWindow().Document()

func main() {
	domTarget := document.GetElementByID("app")

	react.Render(App(AppProps{EntryState: state.State.Root().Entries(),
		ErrorState: state.State.Root().Errors()}), domTarget)
}
