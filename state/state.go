package state

import "nvidia.com/kv-frontend/model"

//go:generate stateGen

var State = NewRoot()

var root _Node_App

type _Node_App struct {
	ActiveModal *model.Modals
	Root        *_Node_Data
}

type _Node_Data struct {
	Entries *model.Entries
	Errors  *model.Errors
}
