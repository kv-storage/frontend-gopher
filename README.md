# Simple KV Web UI GopherJS React Rework
Built with [React](https://github.com/myitcv/react) and [GopherJS](https://github.com/gopherjs/gopherjs).

## Dependancies
```
go get -u github.com/gopherjs/gopherjs
go get -u myitcv.io/{react,react/cmd/reactGen}
go get -u myitcv.io/immutable
```
amend PATH for gopherjs and reactGen
```
export PATH="$(dirname $(go list -f '{{.Target}}' myitcv.io/react/cmd/reactGen)):$PATH"
```
## Development server

Run `gopherjs serve` for a dev server. Navigate to `http://localhost:8080/nvidia.com/kv-frontend`.
